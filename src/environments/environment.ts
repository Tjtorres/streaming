// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  activateStreamingMock:true,
  firebaseConfig:{
    apiKey: "AIzaSyCJedaIW_hyIC9i4Bx2mLgRPpB0OQ_jo1c",
    authDomain: "streaming-64d10.firebaseapp.com",
    projectId: "streaming-64d10",
    storageBucket: "streaming-64d10.appspot.com",
    messagingSenderId: "927440988447",
    appId: "1:927440988447:web:48f727bfc64921cb33a82a",
    measurementId: "G-M6C32F7WPF"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
