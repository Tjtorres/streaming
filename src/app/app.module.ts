import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LoginComponent } from './components/login/login.component';

import { FormsModule } from '@angular/forms';
import { AngularFireModule } from '@angular/fire/compat';
import { HttpClientModule } from '@angular/common/http';
import { environment } from '../environments/environment';
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from './components/template/navbar/navbar.component';
import { StreamingComponent } from './components/home/streaming/streaming.component';
import { StreamingSliderComponent } from './components/home/streaming/streaming-slider/streaming-slider.component';
import { StreamingDetailComponent } from './components/home/streaming/streaming-detail/streaming-detail.component';

import { StreamingService } from './services/streaming-services/streaming.service';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    NavbarComponent,
    StreamingComponent,
    StreamingSliderComponent,
    StreamingDetailComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
  ],
  providers: [StreamingService],
  bootstrap: [AppComponent]
})
export class AppModule { }
