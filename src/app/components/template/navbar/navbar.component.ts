import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(private autenticationService:AuthService,private router: Router) { }

  ngOnInit(): void {
  }

  logout(){

    this.autenticationService.logout();
    this.router.navigate(['login']);
  }
}
