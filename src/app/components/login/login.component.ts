import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { AuthService } from '../../services/auth.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user ={
    nameUser:'',
    password:''
  }

  constructor(private autenticationService:AuthService,private router: Router) { }

  ngOnInit(): void {
  this.autenticationService.getUserLogged().subscribe((user:any)=>{
        if(user){
          sessionStorage.setItem('token',user.uid);
          this.router.navigate(['home']);
        }
  })
  }

  login(){
    this.autenticationService.login(this.user.nameUser,this.user.password).then((res:any)=>{
    });
  }

}
