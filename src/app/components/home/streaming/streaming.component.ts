import { Component, OnInit } from '@angular/core';
import { ProgramType, Program } from 'src/app/models/streaming';
import { StreamingService } from 'src/app/services/streaming-services/streaming.service';

@Component({
  selector: 'app-streaming',
  templateUrl: './streaming.component.html',
  styleUrls: ['./streaming.component.css']
})
export class StreamingComponent implements OnInit {

  itemSelected:any = null;
  selectedDevice:string='';
  series:any=[];
  movies:any=[];
  programs:Program[]=[];

  constructor(private streamingService:StreamingService) { }

  ngOnInit(): void {
    this.streamingService.getStreamingItems().subscribe((res:Program[])=>{
      this.programs = res;
      this.organizeProgramType();
    })
 }

  organizeProgramType(){
    this.series =  this.programs.filter((programType)=> programType.programType === ProgramType.SERIES)
    this.movies =  this.programs.filter((programType)=> programType.programType === ProgramType.MOVIE)

  }

  selectedItem(data:any){
    this.itemSelected = data;
  }

  shortByName(){
    this.series.sort(function (a:Program, b:Program) {
      if (a.title > b.title) {
        return 1;
      }
      if (a.title < b.title) {
        return -1;
      }
      // a must be equal to b
      return 0;
    });

    this.movies.sort(function (a:Program, b:Program) {
      if (a.title > b.title) {
        return 1;
      }
      if (a.title < b.title) {
        return -1;
      }
      // a must be equal to b
      return 0;
    });
  }

  onChange(value:any){
    const nameOrder = value.target.value
    if(nameOrder=== 'nombre'){
      this.shortByName();
    }else if(nameOrder === 'year'){
      this.shortByYear();
    }
  }

  shortByYear(){
    this.series.sort(function (a:Program, b:Program) {
      if (a.releaseYear > b.releaseYear) {
        return 1;
      }
      if (a.releaseYear < b.releaseYear) {
        return -1;
      }
      // a must be equal to b
      return 0;
    });

    this.movies.sort(function (a:Program, b:Program) {
      if (a.releaseYear > b.releaseYear) {
        return 1;
      }
      if (a.releaseYear < b.releaseYear) {
        return -1;
      }
      // a must be equal to b
      return 0;
    });
  }
}
