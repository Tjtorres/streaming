import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-streaming-detail',
  templateUrl: './streaming-detail.component.html',
  styleUrls: ['./streaming-detail.component.css']
})
export class StreamingDetailComponent implements OnInit {

  @Input() dataSelected: any ;

  constructor() { }

  ngOnInit(): void {
  }

}
