import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-streaming-slider',
  templateUrl: './streaming-slider.component.html',
  styleUrls: ['./streaming-slider.component.css']
})
export class StreamingSliderComponent implements OnInit {

   url = 'assets/carousel.js';


  @Input() title:string='';
  @Input() programs: any = {};
  @Output() itemSelected = new EventEmitter<any>();

  constructor() {

  }

  ngOnInit(): void {

  }

  ngAfterViewInit():void{
    const loadAPI = new Promise((resolve) => {
      this.loadScript()

  });
  }

  selectItem(item:any){
    this.itemSelected.emit(item);
  }



  public loadScript() {

      let node = document.createElement('script');
      node.src = this.url;
      node.type = 'text/javascript';
      node.async = true;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
  }
}
