import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StreamingSliderComponent } from './streaming-slider.component';

describe('StreamingSliderComponent', () => {
  let component: StreamingSliderComponent;
  let fixture: ComponentFixture<StreamingSliderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StreamingSliderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StreamingSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
