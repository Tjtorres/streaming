import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

import { map } from "rxjs";
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: "root"
})
export class StreamingService{
  readonly BaseUrl = "https://assets-aivo.s3.amazonaws.com";

  constructor(private http: HttpClient) { }

  StreamingItems(){

    return this.http.get(this.BaseUrl + "/movies.json").pipe(map((res:any)=> res.entries));
  }

  StreamingMocks(){
    return this.http.get('./assets/streaming/streaming.json').pipe(map((res:any)=> res.entries))
  }

  getStreamingItems(){
    if(environment.activateStreamingMock){
      return this.StreamingMocks();
    }else{
     return  this.StreamingItems();
    }
  }
}
