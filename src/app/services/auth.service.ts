import { Injectable } from '@angular/core';
import {AngularFireAuth} from '@angular/fire/compat/auth';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private angularFireAuth:AngularFireAuth) {
 }

 async login(email:string,password:string){
  try {
    return await this.angularFireAuth.signInWithEmailAndPassword(email,password)
  } catch (error) {
    Swal.fire({
      title: 'Error',
      text: 'Usuario y/o contraseña incorrectos',
      icon: 'error',
      confirmButtonText: 'Aceptar'
    })
    return null;
  }
}

getUserLogged(){
  return this.angularFireAuth.authState;
}

logout(){
     sessionStorage.removeItem('token');
     this.angularFireAuth.signOut();
}

}
