import { Injectable } from '@angular/core';
import {  CanActivate, Router } from '@angular/router';
import { map, Observable } from 'rxjs';
import { AuthService } from '../../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private router: Router) {}

  canActivate(): boolean {
    if (sessionStorage.getItem('token')) {
      return true;
    } else {
      this.router.navigateByUrl('login');
      return false;
    }

  }

}
