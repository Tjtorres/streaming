export enum ProgramType{
  MOVIE = 'movie',
  SERIES = 'series'
}

export interface PosterArt {
  url: string;
  width: number;
  height: number;
}

export interface Images {
  PosterArt: PosterArt;
}

export interface Program {
  title: string;
  description: string;
  programType: string;
  images: Images;
  releaseYear: number;
}
